{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty as Scotty
import Data.Aeson
import Data.Monoid (mconcat)
import System.IO
import Data.Char

main = scotty 3000 $
    -- get "/:word" $ do
        -- beam <- param "word"
        -- html $ mconcat ["<h1>Scotty, ", beam, " me up!</h1>"]

    get "/form/:id" $ do
        formId <- param "id"
        let fName = "form_" ++ (formId :: String) ++ ".json"
        content <- readFile fName

        Scotty.json $ object ["word" .= ("ok" :: String)]
